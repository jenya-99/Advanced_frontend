//Creat table:
let elemBody = document.body;
let table = document.createElement('table');
elemBody.insertBefore(table, elemBody.firstChild);
for(let i = 1; i <= 30 ; i++) {
  let newRow = table.insertRow(-1);
  for(let j = 1; j <= 30 ; j++) {
    newRow.insertCell(-1);
  }
}
//Add event listener:
elemBody.addEventListener('click', changeElemColor);
//Function for event listener:
function changeElemColor(e) {
  if (e.target.tagName == 'TD') {
    e.target.classList.toggle('active');
  }
  if (e.target.tagName == 'BODY') {
    table.classList.toggle('active');    
  }
}