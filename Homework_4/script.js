// Класс, объекты которого описывают параметры гамбургера: 
class Hamburger {
  constructor(size, stuffing) {
    try {
      if ((!size && !stuffing) || ((size) && !('size' in size) && !stuffing)) {
        throw new HamburgerException('no size given');
      }
      else if (('size' in size) && !stuffing) {
        throw new HamburgerException('no stuffing given');
      }
      else if ((size) && (stuffing) && !('size' in size)) {
        throw new HamburgerException(`invalid size 'topping_${size.topping}'`);
      }
      else if ((size) && (stuffing) && !('stuffing' in stuffing)) {
        throw new HamburgerException(`invalid stuffing 'topping_${stuffing.topping}'`);
      }
      else {
        this.sizeObj = size;
        this.stuffingObj = stuffing;
        this.toppings = [];
      }
    } catch(err) {
      err.showMessage(err.name, err.message);
    }  
  }
  // Добавить добавку к гамбургеру. Можно добавить несколько
  // добавок, при условии, что они разные: 
   addTopping(topping) { 
    try { 
      if ((this.toppings.length > 0 && 
          this.toppings[0].topping == topping.topping) ||
         (this.toppings.length > 1 && 
          this.toppings[1].topping == topping.topping)) {        
            throw new HamburgerException(`duplicate topping 'topping_${topping.topping}'`);
      }
      else this.toppings.push(topping);
    } catch(err) {
        err.showMessage(err.name, err.message);
      }
  }
  // Убрать добавку, при условии, что она ранее была добавлена:
  removeTopping(topping) {
    try {      
      if ((this.toppings.length > 0 && 
        this.toppings[0].topping == topping.topping) ||
       (this.toppings.length > 1 && 
        this.toppings[1].topping == topping.topping)) {
          this.toppings.splice(this.toppings.indexOf(topping), 1);
          }  
      else {
        throw new HamburgerException(`no 'topping_${topping.topping}' for remove`);
      }
    } catch(err) {
      err.showMessage(err.name, err.message);
    }   
  }
  // Получить массив добавленных добавок, который
  // содержит константы Hamburger.topping_*  :  
  get toppingsArr() {
    return this.toppings;
  }
  // Узнать размер гамбургера:  
  getSize() {
    return this.sizeObj;
  }
  // Узнать начинку гамбургера:  
  getStuffing() {
    return this.stuffingObj;
  }
  // Узнать цену гамбургера:
  calculatePrice() {
    let toppingsSum = 0;  
    hamburger.toppingsArr.forEach(function(elem) {
    toppingsSum += +elem.price;
    });
    return +hamburger.getSize().price +
           +hamburger.getStuffing().price +
           toppingsSum;
  }
  // Узнать калорийность гамбургера:
  calculateCalories() {
    let toppingsCalories = 0;
    hamburger.toppingsArr.forEach(function(elem) {
      toppingsCalories += +elem.calories;
    });
    return +hamburger.getSize().calories +
            +hamburger.getStuffing().calories +
            toppingsCalories;
  }
  // Константы: размеры, виды начинок и добавок:
  static get size_small() {return {size: 'small', price: 50, calories: 20};}
  static get size_large() {return {size: 'large', price: 100, calories: 40};}
  static get stuffing_cheese() {return {stuffing:'cheese', price: 10, calories: 20};}
  static get stuffing_salad() {return {stuffing:'salad', price: 20, calories: 5};}
  static get stuffing_potato() {return {stuffing:'potato', price: 15, calories: 10};}
  static get topping_spise() {return {topping:'spise', price: 15, calories: 0};}
  static get topping_mayo() {return {topping:'mayo', price: 20, calories: 5};}
}
// Представляет информацию об ошибке в ходе работы с гамбургером. 
// Подробности хранятся в свойстве message:
class HamburgerException {
  constructor(message) {
    this.message = message;
    this.name = "HamburgerException:";
  }  
  showMessage (name, message) {
      console.log(`%c ${name} ${message}`, "color: darkorange");
  }
}
//Using examples:
let hamburger = new Hamburger(Hamburger.size_small, Hamburger.stuffing_potato);
hamburger.addTopping(Hamburger.topping_spise);
hamburger.addTopping(Hamburger.topping_mayo);
// hamburger.removeTopping(Hamburger.topping_mayo);
console.log(hamburger);
console.log(`Цена гамбургера: ${hamburger.calculatePrice()} грн.`);
console.log(`Калорий: ${hamburger.calculateCalories()} Кал.`);