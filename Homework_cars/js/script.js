(function() {
    let cars = [];
    let selectedYear;
    let selectedMake;
    fetch("cars.json")
    .then((response) => {    
    return response.json();
    })
    .then((data) => {
        cars = data;
        const years = [...new Set(data.map(element => element.year))];
        let frag = document.createDocumentFragment();
        years.forEach((item) => {
            const year = document.createElement("option");            
            year.innerText = item;
            year.value = item;
            frag.appendChild(year);
        });
        let selectYear = document.getElementById('year');
        selectYear.appendChild(frag);
        selectYear.disabled = false;

        return new Promise(function(resolve) {
            selectYear.onchange = () => {
                selectedYear = selectYear.value;   
                resolve(selectedYear);
            };        
        });      
    })   
    .then(function(selectedYear) {
        document.getElementById('year').disabled = true;
        let selectMake = document.getElementById('make');
        selectMake.disabled = false;
        let makeArr = cars.filter(elem => {
            return elem.year == selectedYear
        });
        let makeUnicum = new Set();
        makeArr.forEach(item => {
            makeUnicum.add(item.make);
        });
        let frag = document.createDocumentFragment();
        makeUnicum.forEach(item => {
            const make = document.createElement("option");            
            make.innerText = item;
            make.value = item;
            frag.appendChild(make);
        });
        selectMake.appendChild(frag);
        selectMake.disabled = false;

        return new Promise(function(resolve) {
            make.onchange = () => {
                selectedMake = selectMake.value;   
                resolve(selectedMake);
            };        
        }); 
    })
    .then(function(selectedMake) {
        document.getElementById('make').disabled = true;
        let selectModel = document.getElementById('model');
        selectModel.disabled = false;

        const modelArr = [];
        cars.forEach(item => {
            if (item.year == selectedYear && item.make == selectedMake) {
                modelArr.push(item.model)                
            }            
        });

        let frag = document.createDocumentFragment();
        modelArr.forEach(item => {
            let model = document.createElement("option");            
            model.innerText = item;
            model.value = item;
            frag.appendChild(model);
        });
        selectModel.appendChild(frag);
        selectModel.disabled = false;

        return new Promise(function(resolve) {
            model.onchange = () => {
                let selectedModel = selectModel.value;   
                resolve(selectedModel);
            };        
        });
    })
    .then(function(selectedModel) {
        document.getElementById('model').disabled = true;
        let container = document.createElement('div');
        container.innerHTML = `<h1>Выбранный автомобиль:</h1>
        <h2>Производитель: ${selectedMake}</h2>
        <h2>Модель: ${selectedModel}</h2>
        <h2>Год выпуска: ${selectedYear}</h2>`;   
        let button = document.getElementById('button');
        button.onclick = () => document.body.appendChild(container);        
    });
    //For reset selected data:
    let reset = document.getElementById('reset');
    reset.onclick = () => location.reload();    
})();