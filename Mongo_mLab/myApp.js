const express = require("express");
const MongoClient = require("mongodb").MongoClient;
const objectId = require("mongodb").ObjectID;
   
const app = express();
const jsonParser = express.json();

// создаем объект MongoClient и передаем ему строку подключения:
const url = "mongodb+srv://jenya-99:krechetov-1966@usersdb-sgtac.gcp.mongodb.net/test?retryWrites=true";
//const url = "mongodb://localhost:27017/"; - для локальной БД
const mongoClient = new MongoClient(url, { useNewUrlParser: true });
 
let dbClient;
 
// app.use(express.static(__dirname + "/public"));
app.use(express.static(__dirname));

mongoClient.connect(function(err, client) {
  if (err) return console.log(err);
  dbClient = client;
  app.locals.collection = client.db("shopDataBase").collection("products");
  app.listen(3000, function() {
    console.log("Сервер подключен...");
  });
});

//Когда приходит GET-запрос к приложению,
//возвращаем в ответ клиенту все документы из базы данных:
app.get("/products", function(req, res) {
  const collection = req.app.locals.collection;
  collection.find({}).toArray(function(err, products) {       
    if (err) return console.log(err);
    res.send(products);
  });   
});

//Если в GET-запросе передается параметр id, то возвращаем
//только однин товар из базы данных (по его id):
app.get("/products/:id", function(req, res) {        
  const id = new objectId(req.params.id);
  const collection = req.app.locals.collection;
  collection.findOne({_id: id}, function(err, product) {             
    if(err) return console.log(err);
    res.send(product);
  });      
});

//Когда приходит POST-запрос, с помощью парсера jsonParser
//получаем отправленные данные и по ним создаем объект, который
//добавляем в базу данных посредством метода insertOne():
app.post("/products", jsonParser, function (req, res) {       
  if(!req.body) return res.sendStatus(400);   
  const productCode = req.body.code;
  const productName = req.body.name;
  const productQnt = req.body.qnt;
  const product = {code: productCode, name: productName, qnt: productQnt};     
  const collection = req.app.locals.collection;
  collection.insertOne(product, function(err, result) {             
    if(err) return console.log(err);
    res.send(product);
  });
});

//В методе app.delete(), который срабатывает при получении запроса DELETE,
//вызываем метод findOneAndDelete() для удаления данных:
app.delete("/products/:id", function(req, res) {        
  const id = new objectId(req.params.id);
  const collection = req.app.locals.collection;
  collection.findOneAndDelete({_id: id}, function(err, result) {             
    if(err) return console.log(err);    
    let product = result.value;
    res.send(product);
  });
});

//При получении PUT-запроса получаем отправленные данные и с помощью
//метода findOneAndUpdate() обновляем данные в БД:
app.put("/products", jsonParser, function(req, res) {      
  if (!req.body) return res.sendStatus(400);
  const id = new objectId(req.body.id);
  const productCode = req.body.code;
  const productName = req.body.name;
  const productQnt = req.body.qnt;     
  const collection = req.app.locals.collection;
  collection.findOneAndUpdate({_id: id}, {$set: {code: productCode, name: productName, qnt: productQnt}},
    {returnOriginal: false}, function(err, result) {            
    if(err) return console.log(err);     
    const product = result.value;
    res.send(product);
  });
});

// прослушиваем прерывание работы программы (ctrl-c)
process.on("SIGINT", () => {
  dbClient.close();
  process.exit();
});