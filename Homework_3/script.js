/**
* Класс, объекты которого описывают параметры гамбургера. 
* 
* @constructor
* @param size        Размер
* @param stuffing    Начинка
* @throws {HamburgerException}  При неправильном использовании
*/
function Hamburger(size, stuffing) {
  try {
    if ((!size && !stuffing) || ((size) && !('size' in size) && !stuffing)) {
      throw new HamburgerException('no size given');
    }
    else if (('size' in size) && !stuffing) {
      throw new HamburgerException('no stuffing given');
    }
    else if ((size) && (stuffing) && !('size' in size)) {
      throw new HamburgerException("invalid size 'topping_" + size.topping + "'");
    }
    else if ((size) && (stuffing) && !('stuffing' in stuffing)) {
      throw new HamburgerException("invalid stuffing 'topping_" + stuffing.topping + "'");
    }
    else {
      this.sizeObj = size;
      this.stuffingObj = stuffing;
      this.toppings = [];
    }
  } catch(err) {
    err.showMessage(err.name, err.message);
  }
}
/* Константы: размеры, виды начинок и добавок */
Hamburger.size_small = {size: 'small', price: 50, calories: 20};
Hamburger.size_large = {size: 'large', price: 100, calories: 40};
Hamburger.stuffing_cheese = {stuffing:'cheese', price: 10, calories: 20};
Hamburger.stuffing_salad = {stuffing:'salad', price: 20, calories: 5};
Hamburger.stuffing_potato = {stuffing:'potato', price: 15, calories: 10};
Hamburger.topping_spise = {topping:'spise', price: 15, calories: 0};
Hamburger.topping_mayo = {topping:'mayo', price: 20, calories: 5};
Object.freeze(Hamburger);
/**
* Добавить добавку к гамбургеру. Можно добавить несколько
* добавок, при условии, что они разные.
* 
* @param topping     Тип добавки
* @throws {HamburgerException}  При неправильном использовании
*/
Hamburger.prototype.addTopping = function(topping) {
  try {
    if (this.toppings.indexOf(topping) != -1) {
      throw new HamburgerException("duplicate topping 'topping_" + topping.topping + "'");
    }
    else this.toppings.push(topping); 
  } catch(err) {
    err.showMessage(err.name, err.message);
  }  
}
/**
 * Убрать добавку, при условии, что она ранее была 
 * добавлена.
 * 
 * @param topping   Тип добавки
 * @throws {HamburgerException}  При неправильном использовании
 */
Hamburger.prototype.removeTopping = function(topping) {
  try {
    if (this.toppings.indexOf(topping) != -1) {
      this.toppings.splice(this.toppings.indexOf(topping), 1);
    }  
    else {
      throw new HamburgerException("no 'topping_" + topping.topping + "' for remove");
    }
  } catch(err) {
    err.showMessage(err.name, err.message);
  }   
}
/**
 * Получить список добавок
 *
 * @return {Array} Массив добавленных добавок, содержит константы
 *                 Hamburger.TOPPING_*
 */
Hamburger.prototype.getToppings = function() {
  return this.toppings;
}
/**
 * Узнать размер гамбургера
 */
Hamburger.prototype.getSize = function() {
  return this.sizeObj;
}
/**
 * Узнать начинку гамбургера
 */
Hamburger.prototype.getStuffing = function() {
  return this.stuffingObj;
}
/**
 * Узнать цену гамбургера
 * @return {Number} Цена
 */
Hamburger.prototype.calculatePrice = function() {
  var toppingsArr = hamburger.getToppings();
  var toppingsSum = 0;  
  toppingsArr.forEach(function(elem) {
  toppingsSum += +elem.price;
  });
  return +hamburger.getSize().price +
         +hamburger.getStuffing().price +
         toppingsSum;
}
/**
 * Узнать калорийность гамбургера
 * @return {Number} Калорийность
 */
Hamburger.prototype.calculateCalories = function() {
  var toppingsArr = hamburger.getToppings();
  var toppingsCalories = 0;
  toppingsArr.forEach(function(elem) {
    toppingsCalories += +elem.calories;
  });
  return +hamburger.getSize().calories +
         +hamburger.getStuffing().calories +
         toppingsCalories;
}
/**
 * Представляет информацию об ошибке в ходе работы с гамбургером. 
 * Подробности хранятся в свойстве message.
 * @constructor 
 */
function HamburgerException(message) {
  this.message = message;
  this.name = "HamburgerException:";
  this.showMessage = function(name, message) {
    console.log("%c" + name + " " + message, "color: darkorange");
  }
}
//Using examples:
var hamburger = new Hamburger(Hamburger.size_small, Hamburger.stuffing_cheese);
hamburger.addTopping(Hamburger.topping_mayo);
hamburger.addTopping(Hamburger.topping_spise);
hamburger.removeTopping(Hamburger.topping_mayo);
console.log(hamburger);
console.log("Цена гамбургера: " + hamburger.calculatePrice() + " грн.");
console.log("Калорий: " + hamburger.calculateCalories() + " Кал.");